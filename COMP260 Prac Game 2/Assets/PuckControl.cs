﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	private AudioSource audio;
	public Transform startingPos;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody> ();
		ResetPosition (); 
	}
	void OnCollisionEnter (Collision collision) {
		if (paddleLayer.Contains (collision.gameObject)) {
			
			GetComponent<AudioSource>().PlayOneShot (paddleCollideClip);
		} else {
			
			GetComponent<AudioSource>().PlayOneShot (wallCollideClip);
		}
		Debug.Log ("Collision Enter" + collision.gameObject.name);
	}

	void OnCollisionStay (Collision collision) {
		Debug.Log ("Collision Stay" + collision.gameObject.name);
	}

	void OnCollisionExit (Collision collision) {
		Debug.Log ("Collision Exit" + collision.gameObject.name);
	}
	public void ResetPosition () {
		rigidbody.MovePosition (startingPos.position);
		rigidbody.velocity = Vector3.zero;

	// Update is called once per frame
	void Update () {
		
	}
}
