﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof(AudioSource))]
public class Goal : MonoBehaviour {

	public AudioClip scoreClip;
	private AudioSource audio;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	
	}

	void OnTriggerEnter (Collider collider) {
		audio.PlayOneShot (scoreClip);
	// Update is called once per frame
		PuckControl puck = 
			collider.gameObject.GetComponent<PuckControl> ();
		puck.ResetPosition ();
	}
	void Update () {
	
	}
}
