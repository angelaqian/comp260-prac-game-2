﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class MovePaddle2 : MonoBehaviour {

	private new Rigidbody rigidbody;
	// private Vector3 GetMousePosition() {
	 //	Ray ray = 
	//		Camera.main.ScreenPointToRay (Input.mousePosition);
	//	Plane plane = new Plane (Vector3.forward, Vector3.zero);
	//	float distance = 0;
	//	plane.Raycast (ray, out distance);
	//	return ray.GetPoint (distance);
	//}
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	
	}
	public float speed = 20f; 

	void FixedUpdate () {
		// Vector3 pos = GetMousePosition ();
		Vector2 direction = Vector2.zero;
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");
		// Vector3 dir = pos - rigidbody.position;
		Vector3 vel = direction.normalized * speed;

		// float move = speed * Time.fixedDeltaTime;
		// float distToTarget = dir.magnitude;

		// if (move > distToTarget) {
		//	vel = vel * distToTarget / move;
		// }
		rigidbody.velocity = vel;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
